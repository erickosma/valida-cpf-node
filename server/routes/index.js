var express = require('express');
var router = express.Router();
var db = require('../src/db/sqlite');


router.use(function (req, res, next) {
    db.countRequest();
    next();
});


/* GET home page. */
router.get('/', function (req, res, next) {
    res.jsonp({
        ping: "Ok",
        status: "Ok"
    });
});

router.post('/blacklist', function (req, res, next) {
    try {
        var cpf = req.body.cpf;
        db.insert(cpf);
        res.json({message: 'Adicionado!'});
    }
    catch(err) {
       console.log(err);
        res.json({message: 'error'});
    }

});


router.delete('/blacklist', function (req, res, next) {
    try {
        var cpf = req.body.cpf;
        db.deletCpf(cpf);
        res.json({message: 'Removido!'});
    }
    catch(err) {
        console.log(err);
        res.json({message: 'error'});
    }

});


router.get('/consulta', function (req, res, next) {
    var cpf  = req.query.cpf;
    console.log("consulta cpf"+cpf);
    if(!!cpf){
        db.get("SELECT cpf FROM blacklist where cpf = '"+cpf+"'", function(err, row){
            if(err){
                res.json(err);
            }
            var ret = 'BLOCK';
             if(!row){
                ret = 'FREE';
            }
            res.json({ "message" :ret });
        });
        //http://127.0.0.1:3000/consulta?cpf=00000000000
    }
    else{
        res.json({ "message":'FREE'});
    }

});

router.get('/status', function (req, res, next) {

    function countVlr(retVal){
        res.json({total_cpf:retVal.total,requestsCount: retVal.request,upTime: retVal.uptime});;
    }
    db.totalCpf(countVlr);

});


module.exports = router;
