var express = require('express');
var sqlite3 = require('sqlite3').verbose();
var   db = new sqlite3.Database('dbteste.db');
/*try {
     db = new sqlite3.Database('dbteste.db');
}
catch(err) {
     db = new sqlite3.Database(':memory');
}*/


/**
 * get date and time
 */
db.datetime = function(){
    var currentdate = new Date();
    var datetime = '' + currentdate.getDate() + "/"
        + (currentdate.getMonth()+1)  + "/"
        + currentdate.getFullYear() + " "
        + currentdate.getHours() + ":"
        + currentdate.getMinutes() + ":"
        + currentdate.getSeconds();
    return datetime;
}


/**
 * Insert
 * @param cpf
 */
db.insert =  function(cpf){
    var stmt = this.prepare('INSERT INTO blacklist VALUES (?,?)');
    stmt.run(cpf,this.datetime());
    stmt.finalize();
}

db.deletCpf =  function(cpf){
    if(!!cpf){
        var stmt = this.prepare('DELETE FROM blacklist WHERE cpf = ?');
        stmt.run(cpf);
        stmt.finalize();
    }
}


db.totalCpf = function (callBack){
    var vlTotal = [];
    var sql ="SELECT\n" +
        "  count(*) AS total,\n" +
        "  rq.total AS reqt,\n" +
        "  rq.created_at\n" +
        "FROM blacklist AS b\n" +
        "LEFT JOIN request rq";
    this.each(sql, function(err, row) {
        vlTotal['total'] = (!!row.total) ?  row.total  : 0;
        vlTotal['request'] = (!!row.reqt) ?  row.reqt  : 0;
        vlTotal['uptime'] = (!!row.created_at) ?  row.created_at  : 0;
        return callBack(vlTotal);
    });
};

db.countRequest = function (){
    this.run("UPDATE request SET total = total+1 WHERE 1");
};

db.getCountRequest = function (callBack){
    var vlTotal = 0;
    var dateTime=0;
    this.each("SELECT total as reqt,created_at FROM request", function(err, row) {
        vlTotal = (!!row.reqt) ?  row.reqt  : 0;
        dateTime = (!!row.created_at) ?  row.created_at  : 0;
        return callBack(vlTotal,dateTime);
    })
};



db.serialize(function() {

    db.run("CREATE TABLE if not exists blacklist (cpf TEXT, created_at TEXT)");
    db.run("CREATE TABLE if not exists request (total INTEGER,created_at TEXT)");
    db.run('DELETE FROM request WHERE 1');
    var stmtRq = db.prepare("INSERT INTO request VALUES (?,?)");
    stmtRq.run(0,db.datetime());

    db.each("SELECT count(*) as total FROM blacklist", function(err, row) {
        var total = (!!row.total) ?  row.total  : 0;
        console.log("Total cpf: " + total);
        if(parseInt(total) === 0){
            /**
             * insert inicial
             */
            var stmt = db.prepare("INSERT INTO blacklist VALUES (?,?)");
            for (var i = 0; i < 10; i++) {
                var cpf= '' +i+ ''+i+ ''+i+ ''+i+ ''+i+ ''+i+ ''+i+ ''+i+ ''+i+ ''+i+ ''+i+ '';
                stmt.run(cpf,db.datetime());
            }
            stmt.finalize();
        }
    });

});

module.exports = db;