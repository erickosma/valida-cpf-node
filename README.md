Aplica��o Node.js 
===================

Aplica��o Node.js que seja acess�vel localmente e verifique se um determinado n�mero de CPF est� em uma Blacklist. 

A aplica�� est� dividida em 

```
valida-cpf-node
-- client
-- server
``` 
**client**  Diret�rio contendo interface usu�rio do projeto.
Para constru��o foi utilizado o frmework [Vue.js](https://vuejs.org//)

**server**  Diret�rio contendo api do projeto.
Para constru��o foi utilizado o frmework [Express](http://expressjs.com/pt-br/) 





##Estrutura  ##


```
valida-cpf-node
|-- client
|   |-- build         // Scripts e inicializa��o
|   |-- config  
|   |-- src           //Regras de neg�cio
|   `-- test
`-- server
    |-- bin           //Scripts e inicializa��o
    |-- routes        //Rotas do sistema
    `-- src           //Regras de neg�cio
        |-- db
        `-- models
``` 




##instala��o ##


```bash
clone git@bitbucket.org:erickosma/valida-cpf-node.git
``` 

###instalar as depend�ncias### 

```bash
$ cd valida-cpf-node  
$ npm install  

``` 



###Testes
Para os testes foram usados 
A lib **vue-test-utils**

A biblioteca vue-test-utils � a lib oficial para 
testes unit�rios com vuejs.  Baseada na lib avoriaz . 


Testes de Integra��o e Testes E2E
Para verificar se a api esta funcionando 


Para rodar os testes 
```bash
$ cd valida-cpf-node  
$ npm run test
```


###Rodando o projeto###

Rodando o projeto via linha de comando 

```bash
$ npm run start
```

Ou rodar via docker 

```bash
$ docker-compose build
$ docker-compose run web 
```


Rodando a api separadamente 


```bash
$ cd server 
$ npm run start
```


Rodando a insterface separada 


```bash
$ cd client 
$ npm run start
```



####Url e rotas####

***api***

Ping
```
localhost:3000/  ->  GET  /
```
Adiciona na blcklist
```
localhost:3000/blacklist/ -> POST

curl -X POST --data "cpf=0833333333" localhost:3000/blacklist
```

Removendo da lista  


```
localhost:3000/blacklist/ -> DELETE

curl -X DELETE --data "cpf=0833333333" localhost:3000/blacklist
```


Consulta cpf 

```
localhost:3000/consulta/?cpf=0000000000000 -> GET

curl -X GET --data "cpf=0833333333" localhost:3000/consulta
```


