import Vue from 'vue'
import HelloWorld from '@/components/HelloWorld'

describe('HelloWorld.vue', () => {
  it('Deve renderizar o conteudo inicial', () => {
    const Constructor = Vue.extend(HelloWorld)
    const vm = new Constructor().$mount()
      console.log(vm.$el.querySelector('.hello'));
    expect(vm.$el.querySelector('h1').textContent)
    .toEqual('Bem Vindo')
  })
})
