import axios from 'axios'

const HOST = process.env.API_HOST || "localhost"
const PORT = process.env.API_PORT && Number(process.env.API_PORT) || 3000

export default() => {
    return axios.create({
        baseURL: HOST+':'+PORT
    })
}